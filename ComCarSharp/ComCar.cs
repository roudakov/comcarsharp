﻿using System;
using System.Text;

using System.IO;
using System.Xml;
using System.Runtime.InteropServices;
using System.Net;
using System.Collections.Generic;

using System.Drawing;
using System.Drawing.Imaging;

namespace ComCarSharp
{

    [Guid("12042DF2-5FB4-425A-9E49-063F5D414A3E")]
    internal interface IRecognizer
    {
        [DispId(1)]
        bool isImageOk { get; }
        [DispId(2)]
        int maxPlateSym { get; set; }
        [DispId(3)]
        int maxPlateArea { get; set; }
        [DispId(4)]
        int minPlateArea { get; set; }


        [DispId(5)]
        int countPlates { get; }
        
        [DispId(6)]
        string plate(int idx);
        
        [DispId(7)]
        void openFile(string filename);
        [DispId(8)]
        void openURL(string url, string username, string password);
        [DispId(9)]
        void saveFile(string path);

        [DispId(10)]
        bool analizeImage();
        [DispId(11)]
        void drawInfo();

        [DispId(12)]
        void loadLicense();
        [DispId(13)]
        string version();
    }


    [Guid("0C8A8CFB-8006-43EE-B3AF-B0B1D59622C9"), InterfaceType(ComInterfaceType.InterfaceIsIDispatch)]
    public interface IComCarEvents
    {
    }


    [Guid("3A8BFFFD-9C0E-47FF-A035-916D3AA439E6"), ClassInterface(ClassInterfaceType.None), ComSourceInterfaces(typeof(IComCarEvents))]
    public class Recognizer :IRecognizer
    {
        public struct ANPR_OPTIONS
        {
            public int min_plate_size; // Минимальная площадь номера
            public int max_plate_size; // Максимальная площадь номера
            public int Detect_Mode; // Режимы детектирования	
            public int max_text_size; // Максимальное количество символов номера + 1
            public int type_number; // Тип автомобильного номера	
            public int flags; // Дополнительные опции
        };

#if WIN32
        [DllImport("iANPRinterface_vc12_x86.dll", CallingConvention = CallingConvention.StdCall)]
        unsafe public static extern int anprPlateMemoryXML(byte[] in_buffer, int size_buffer, ANPR_OPTIONS Options, 
            StringBuilder xml_buffer, int[] size_xml_buffer);
        [DllImport("iANPR_vc12_x86.dll", CallingConvention = CallingConvention.StdCall)]
        unsafe public static extern void LicenseValue(sbyte[] key);

#elif WIN64
        [DllImport("iANPRinterface_vc12_x64.dll", CallingConvention = CallingConvention.StdCall)]
        unsafe public static extern int anprPlateMemoryXML(byte[] in_buffer, int size_buffer, ANPR_OPTIONS Options, 
            StringBuilder xml_buffer, int[] size_xml_buffer);
        [DllImport("iANPR_vc12_x64.dll", CallingConvention = CallingConvention.StdCall)]
        unsafe public static extern void LicenseValue(sbyte[] key);
#endif


        byte[] _image = null;

        int _maxPlateSym = 10;
        int _maxPlateArea = 8900;
        int _minPlateArea = 5500;
        const int _maxNumbersOnImage = 10;

        List<string> plates = new List<string>();
        List<Rectangle> coords = new List<Rectangle>();

        public bool isImageOk
        {
            get { return _image != null; }
        }

        public int maxPlateSym
        {
            get { return _maxPlateSym; }
            set { _maxPlateSym = value; }
        }
        public int maxPlateArea
        {
            get{ return _maxPlateArea; }
            set{ _maxPlateArea = value; }
        }
        public int minPlateArea
        {
            get{ return _minPlateArea; }
            set{ _minPlateArea = value; }
        }

        public void openFile(string filename)
        {
            using (FileStream file = new FileStream(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                readStream(file);
            }
        }

        public void openURL(string url, string username = null, string password = null)
        {
            var web = WebRequest.Create(url);

            if (username == null || password == null)
                web.Credentials = CredentialCache.DefaultCredentials;
            else
                web.Credentials = new NetworkCredential(username, password);

            using (var response = (HttpWebResponse)web.GetResponse())
            {
                using (Stream dataStream = response.GetResponseStream())
                {
                    readStream(dataStream);
                }
            };
        }

        private void readStream(Stream stream)
        {
            using (var binr = new BinaryReader(stream))
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    var cache = new byte[4096];
                    int read;

                    while ((read = binr.Read(cache, 0, cache.Length)) > 0)
                    {
                        ms.Write(cache, 0, read);
                    }
                    _image = ms.GetBuffer();
                }
            }
        }

        public bool analizeImage()
        {
            ANPR_OPTIONS anpr;
            anpr.Detect_Mode = 14;
            anpr.min_plate_size = 500;// _minPlateArea;
            anpr.max_plate_size = 5000;//_maxPlateArea;
            anpr.max_text_size = _maxPlateSym;
            anpr.type_number = 0;
            anpr.flags = 1;

            StringBuilder buffer_builder = new StringBuilder(10000);
            int[] size_builder = new int[1];
            size_builder[0] = 10000;

            int res = anprPlateMemoryXML(_image, _image.Length, anpr, buffer_builder, size_builder);

            if (res == 0)
            {

                plates.Clear();
                coords.Clear();

                using (var reader = XmlReader.Create(new StringReader(buffer_builder.ToString())))
                {
                    reader.ReadToFollowing("allnumbers");
                    reader.MoveToFirstAttribute();
                    string numbers = reader.Value; // Количество найденных номеров: numbers

                    int all = Convert.ToInt32(numbers);
                    for (int i = 0; i < all; i++)
                    {
                        reader.ReadToFollowing("number");
                        reader.MoveToFirstAttribute();
                        string num = reader.Value; //распознанный номер
                        plates.Add(num);

                        reader.MoveToNextAttribute();
                        string num_coord = reader.Value; //координаты
                        var rect = rectFromString(num_coord);

                        coords.Add(rect);
                    }
                }
                return true;
            }
            else
                return false;
        }
        public int countPlates { 
            get { return plates.Count; } 
        }
        
        public string plate(int idx){
            if (idx < plates.Count)
                return plates[idx];
            else 
                return "";
        }

        public void saveFile(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            using (FileStream file = new FileStream(path, FileMode.CreateNew, FileAccess.Write, FileShare.None))
            {
                file.Write(_image, 0, _image.Length);
            }

        }

        public void loadLicense()
        {
            const string fileName = "lic.key";
            if (File.Exists(fileName))
            {
                sbyte[] key = new sbyte[1581];
                using (BinaryReader reader = new BinaryReader(File.Open(fileName, FileMode.Open)))
                {
                    for (int i = 0; i < reader.BaseStream.Length; i++)
                        key[i] = reader.ReadSByte();
                }
                LicenseValue(key);
            }
            else
                Console.WriteLine("WARNING! File lic.key not found. This may crash program if you use license version of iANPR SDK dlls");

        }

        public string version()
        {
            return "ComCarSharp.Recognizer v1.0";
        }

        Rectangle rectFromString(string str)
        {
            var nums_s = str.Split(',');
            if (nums_s.Length != 4) return new Rectangle();


            var x = Convert.ToInt32(nums_s[0]);
            var y = Convert.ToInt32(nums_s[1]);
            var width = Convert.ToInt32(nums_s[2]);
            var height = Convert.ToInt32(nums_s[3]);
            /*
            var x1 = Convert.ToInt32(nums_s[0]);
            var y1 = Convert.ToInt32(nums_s[1]);
            var x2 = Convert.ToInt32(nums_s[2]);
            var y2 = Convert.ToInt32(nums_s[3]);


            var width= Math.Abs(x2 - x1);
            var height = Math.Abs(y2 - y1);
            var x = Math.Min(x1, x2);
            var y = Math.Min(y1, y2);
            */
            return new Rectangle(x, y, width, height);
        }

        public void drawInfo()
        {
            using (var bin = new MemoryStream(_image))
            {
                using (var img = Image.FromStream(bin))
                {
                    using (var gr = Graphics.FromImage(img))
                    {
                        Pen pen = new Pen(Color.LightGreen);
                        // Create font and brush.
                        Font drawFont = new Font("Arial", 16);
                        SolidBrush drawBrush = new SolidBrush(Color.Black);
                        SolidBrush drawBrush2 = new SolidBrush(Color.LightGreen);
                        StringFormat drawFormat = new StringFormat();
                        //drawFormat.FormatFlags = StringFormatFlags.DirectionVertical;

                        for (int i = 0; i < plates.Count; i++)
                        {
                            var rect = coords[i];
                            var plate = plates[i];

                            gr.DrawRectangle(pen, rect);

                            // Create point for upper-left corner of drawing.
                            var drawPoint = new Point(rect.X + 1, rect.Y + 1 - 25);
                            var drawPoint2 = new Point(rect.X, rect.Y - 25);

                            gr.DrawString(plate, drawFont, drawBrush, drawPoint, drawFormat);
                            gr.DrawString(plate, drawFont, drawBrush2, drawPoint2, drawFormat);
                        }
                    }

                    var memStream = new MemoryStream();
                    img.Save(memStream, ImageFormat.Jpeg);
                    _image = memStream.ToArray();
                }
            }
        }
    }
}
